import com.lagou.model.User;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest {
    private static Connection connection=null;
    private static PreparedStatement preparedStatement=null;
    private static ResultSet resultSet=null;
    public static void main(String[] args) {
        try{
            //加载数据驱动
            Class.forName("com.mysql.jdbc.Driver");
            //通过驱动获取数据库链接
            connection= (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/mybatis?" +
                    "characterEncoding=utf-8","root","1q2w3e");

            select();

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    /**
     * 增加数据
     */
    public static void add() throws SQLException {
        String sql="insert into user values(?,?)";
        preparedStatement=(PreparedStatement) connection.prepareStatement(sql);
        preparedStatement.executeUpdate();
    }

    /**
     * 修改数据
     */
    public static void update() throws SQLException {
        String sql="update user set user values(?) where id=?";
        preparedStatement=(PreparedStatement) connection.prepareStatement(sql);
        preparedStatement.setString(1,"1");
        preparedStatement.setString(2,"1");
        preparedStatement.executeUpdate();

    }

    /**
     * 删除数据
     */
    public static void delete() throws SQLException {
        String sql="delete from user where id=?";
        preparedStatement=(PreparedStatement) connection.prepareStatement(sql);
        preparedStatement.setString(1,"1");
        preparedStatement.executeUpdate();

    }

    /**
     * 查询数据
     */

    public static void select() throws SQLException {
        //定于sql语句
        String sql="select * from user where username=?";
        //获取预处理preparedStatement
        preparedStatement= (PreparedStatement) connection.prepareStatement(sql);
        //设置参数，第一参数从1开始
        preparedStatement.setString(1,"admin");
        //查询结果
        resultSet=preparedStatement.executeQuery();
        //遍历结果集
        while (resultSet.next()){
            User user=new User();
            String guid=resultSet.getString("guid");
            String userName=resultSet.getString("username");
            user.setGuid(guid);
            user.setUserName(userName);
            System.out.println(user);
        }
    }





}
